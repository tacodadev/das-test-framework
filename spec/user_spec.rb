require_relative "../runner"

class User
  def initialize(options = {})
    @email = options[:email]
    @last_login = options[:last_login]
  end

  def self.create(options = {})
    new(options)
  end

  def change_email(email)
    @email = email
  end

  def to_hash
    {
      email: @email,
      last_login: @last_login,
    }
  end

  attr_reader :email, :last_login
end

describe User do
  def user
    @user ||= User.create(email: 'alice@example.com',
                          last_login: Time.new(2015, 10, 21, 10, 22))
  end

  # it 'has an email address' do
  #   user.email.should == 'alice@example.com'
  # end

  # it 'has a last login time' do
  #   user.last_login.should == Time.new(2015, 10, 21, 10, 22)
  # end

  # asserting about the object
  it 'has some attributes' do
    user.email.should == 'alice@example.com'
    user.last_login.should == Time.new(2015, 10, 21, 10, 22)
  end

  # asserting against the object
  it 'has some attributes' do
    user.to_hash.should == {
      email: 'alice@example.com',
      last_login: Time.new(2015, 10, 21, 10, 22)
    }
  end

  it 'can change emails' do
    user.change_email('bob@example.com')
    user.email.should == 'bob@example.com'
  end
end
