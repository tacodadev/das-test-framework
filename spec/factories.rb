module Factories
  module Stripe
    module Customer
      def self.with_valid_card
        # Mirror the module structure for testing
        # But use the actual module
        # Override and stub methods when necessary
        stripe_customer = ::Stripe::Customer.create
        stripe_customer.source = "tok_visa"
        stripe_customer.save
      end
    end

    module User
      def self.admin
        create(admin: true)
      end

      def self.create(params={})
        admin = params.fetch(:admin) { false }
        ::User.create(email: "alice@example.com",
                      sign_up_ip: "127.0.0.1",
                      admin: admin)
      end
    end
  end

  module Subscription
    def self.failed
      create(stripe_customer: Factories::Stripe::Customer.invalid_card)
    end

    def self.create(params={})
      stripe_customer = params.fetch(:stripe_customer) do
        Factories::Stripe::Customer.with_valid_card
      end
      ::Subscription.new(stripe_customer)
    end
  end
end
