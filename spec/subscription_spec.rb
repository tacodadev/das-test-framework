require_relative "../runner"
require_relative "./factories"

class User
  def initialize(options)
    @email = options[:email]
    @sign_up_ip = options[:sign_up_ip]
    @admin = options[:admin]
  end

  def self.create(options={})
    new(options)
  end
end

class Stripe
  class Customer
    def initialize
      @source = "tok_visa"
    end

    def self.create
      new
    end

    def save
    end

    attr_accessor :source
  end
end

class Subscription
  def initialize(stripe_customer)
    @stripe_customer = stripe_customer
  end

  def create!(plan_id)
    @plan_id = plan_id
  end

  def change_plan!(plan_id)
    @plan_id = plan_id
  end

  def plan_id
    @plan_id
  end

  def account_balance
    2_900 - 29_000
  end
end

describe Subscription do
  let(:subscription) do
    stripe_customer = Factories::Stripe::Customer.with_valid_card
    Subscription.new(stripe_customer)
  end

  before { subscription.create!("annual") }

  it "knows its plan id" do
    expect(subscription.plan_id).to eq "annual"
  end

  # NOTE: Try to strive for consistent terminology
  # change over switch
  describe "changing plans" do
    before { subscription.change_plan!("monthly") }

    it "changes the plan" do
      expect(subscription.plan_id).to eq "monthly"
    end

    it "converts remaining time on the previous subscription into credit" do
      expect(subscription.account_balance).to eq(2_900 - 29_000)
    end
  end
end
