require_relative "../runner"

describe "describe" do
  describe "nested describe" do
    it "should still run tests" do
      expect(1 + 1).to eq 2
    end
  end
end

describe "it" do
  it "should not allow its inside other its" do
    expect do
      it "should not work" do
      end
    end.to raise_error(NameError)
  end
end

describe "expectations" do
  it "can expect values" do
    # (1 + 1).should == 2
    expect(1 + 1).to eq 2
  end

  it "can expect exceptions" do
    expect do
      raise ArgumentError
    end.to raise_error(ArgumentError)
  end
end

describe "let" do
  let(:five) { 5 }
  let(:six) { five + 1 }
  let(:random) { rand }

  it "is available inside the tests" do
    expect(five).to eq 5
  end

  it "always returns the same object" do
    expect(random).to eq random
  end

  it "still fails when methods don't exist" do
    expect do
      method_that_doesnt_exist
    end.to raise_error(NameError)
  end

  it "can reference other lets" do
    expect(six).to eq 6
  end

  describe "nested describes" do
    let(:sibling_five) { 5 }

    it "can see lets from the parent describe" do
      expect(five).to eq 5
    end
  end

  describe "sibling describes" do
    it "can't see its sibling's lets" do
      expect do
        sibling_five
      end.to raise_error(NameError)
    end
  end
end

describe "before" do
  before { @five = 5 }
  before { @six = @five + 1 }

  it "should be visible inside the tests" do
    expect(@five).to eq 5
  end

  it "can reference values set up by other befores" do
    expect(@six).to eq 6
  end
end

describe "let and before interacting" do
  let(:five) { 5 }
  before { @six = five + 1 }
  let(:seven) { @six + 1 }

  it "allows befores to reference lets" do
    expect(@six).to eq 6
  end

  it "allows lets to reference befores" do
    expect(seven).to eq 7
  end
end

# Finding tests

# List what it does
# Test how things interact with each other

# Works much better than testing methods

# What it already does:
# - assertions (expectations)
# - describe
# - it
# - lets
# - before

# How do each of these interact with each other?

# Forgoing assertions
# Because they are pretty separate

# Describe interactions
# Itself

# describe + describe   untested  OK
# it + it               untested  BUG: method_missing
# let + let             untested  OK
# before + before       untested  OK

# describe + it         tested    OK
# describe + let        untested  2 BUGS: children, siblings
# describe + before     untested  2 BUGS: children, siblings  TODO

# it + let              tested    OK
# it + before           tested    OK

# let + before          untested  OK
